# Assignment 1 - How to Setup a Portfolio on Netlify

In this tutorial you will find out how to setup your Netlify portfolio.

# Getting Started

These instructions will get you a copy of this project on your local machine for viewing and testing.

### Clone this repository

```
git clone https://mrodrigues95@bitbucket.org/mrodrigues95/info2300-assignment-1.git
```

### Pull this repository

```
git pull https://mrodrigues95@bitbucket.org/mrodrigues95/info2300-assignment-1.git
```

### Or visit this link if you do not wish to have a local copy

```
https://mrodrigues95.github.io/behind-the-scenes/
```

## Built With

* HTML, CSS and JavaScript
* Netlify

## Authors

* **Marcus Rodrigues** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for more details

